package socialnetwork;

import socialnetwork.GUI.MainFX;
import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.Membru;
import socialnetwork.domain.Message;
import socialnetwork.domain.validators.MembruValidator;
import socialnetwork.domain.validators.MessageValidator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.MembruFile;
import socialnetwork.repository.file.MessageFile;

public class Main {
    public static void main(String[] args) {
     /*   String fileNameEchipa=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.echipa");
        String fileNameMessages=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.discutiiCuSefu");


        Repository<Long, Membru> membruFileRepository = new MembruFile(new MembruValidator(), fileNameEchipa);
        Repository<Long, Message> messageRepository=new MessageFile( new MessageValidator(),fileNameMessages, membruFileRepository);


        membruFileRepository.findAll().forEach(System.out::println);
        messageRepository.findAll().forEach(System.out::println);*/

        MainFX.main(args);

    }
}


