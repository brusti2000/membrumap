package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Message extends Entity<Long> {
    private static long idMaxMesaj=0;
    private Membru from;
    private List<Membru> to;
    private String message;
    private LocalDateTime date;



    public Message( Membru from, List<Membru> to, String message, LocalDateTime date) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = date;
        idMaxMesaj++;
        setId(idMaxMesaj);
    }

    public Membru getFrom() {
        return from;
    }

  public String getNameFrom() {return this.from.getName();}
    public List<Membru> getTo() {
        return to;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getDate() {
        return date;
    }
    public int getOra(){return date.getHour();}
    public String getDateString(){return date.format(Constants.DATE_TIME_FORMATER);}

    public void setFrom(Membru from) {
        this.from = from;
    }

    public void setTo(List<Membru> to) {
        this.to = to;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Message{" +
                "from=" + from +
                ", to=" + to +
                ", message='" + message + '\'' +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return Objects.equals(from, message1.from) &&
                Objects.equals(to, message1.to) &&
                Objects.equals(message, message1.message) &&
                Objects.equals(date, message1.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to, message, date);
    }
}
