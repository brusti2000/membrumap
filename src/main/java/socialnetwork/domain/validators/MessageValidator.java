package socialnetwork.domain.validators;

import socialnetwork.domain.Membru;
import socialnetwork.domain.Message;
import socialnetwork.repository.Repository;

public class MessageValidator implements Validator<Message> {
    private Repository<Long, Membru> repositoryUtilizator;
    @Override
    public void validate(Message entity) throws ValidationException {
        String errors="";
        if(entity.getMessage().length() < 1){
            errors+="Invalid message!\n";
        }

        if(errors.length()>0) throw new ValidationException(errors);
    }
}
