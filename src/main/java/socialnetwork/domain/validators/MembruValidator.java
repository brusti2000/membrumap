package socialnetwork.domain.validators;

import socialnetwork.domain.Membru;

public class MembruValidator implements Validator<Membru> {
    @Override
    public void validate(Membru entity) throws ValidationException {
        String errors="";
        if(entity.getName().matches(".*\\d.*") ){
            errors+="Name contains numbers!\n";
        }
        if(entity.getName().equals("") ){
            errors+=" name is null\n";
        }
        if(entity.getName().length()<3){
            errors+="name contains less than 3 letters\n";
        }
        if(errors.length()>0)
            throw new ValidationException(errors);
    }
}
