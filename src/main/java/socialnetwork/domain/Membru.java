package socialnetwork.domain;

import java.util.List;
import java.util.Objects;

public class Membru extends Entity<Long>{
    private String name;
    private String rol;
    private String stare;

    public Membru(String name, String rol, String stare) {
        this.name = name;
        this.rol = rol;
        this.stare = stare;
    }

    public String getName() {
        return name;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getStare() {
        return stare;
    }

    public void setStare(String stare) {
        this.stare = stare;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Membru membru = (Membru) o;
        return Objects.equals(name, membru.name) &&
                Objects.equals(rol, membru.rol) &&
                Objects.equals(stare, membru.stare);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, rol, stare);
    }

    @Override
    public String toString() {
        return "Membru{" +
                "name='" + name + '\'' +
                ", rol='" + rol + '\'' +
                ", stare='" + stare + '\'' +
                '}';
    }
}