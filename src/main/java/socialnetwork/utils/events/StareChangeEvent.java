package socialnetwork.utils.events;


import socialnetwork.domain.Membru;



public class StareChangeEvent implements Event{
    private ChangeEventType eventType;
    private Membru oldStare,newStare;

    public StareChangeEvent(ChangeEventType eventType, Membru oldStare) {
        this.eventType = eventType;
        this.oldStare = oldStare;

    }

    public StareChangeEvent(ChangeEventType eventType, Membru oldStare, Membru newStare) {
        this.eventType = eventType;
        this.oldStare = oldStare;
        this.newStare = newStare;
    }
    public StareChangeEvent() {

    }

    public ChangeEventType getEventType() {
        return eventType;
    }

    public void setEventType(ChangeEventType eventType) {
        this.eventType = eventType;
    }

    public Membru getOldStare() {
        return oldStare;
    }

    public void setOldStare(Membru oldStare) {
        this.oldStare = oldStare;
    }

    public Membru getNewStare() {
        return newStare;
    }

    public void setNewStare(Membru newStare) {
        this.newStare = newStare;
    }
}
