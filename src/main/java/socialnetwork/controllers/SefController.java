package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import socialnetwork.domain.Membru;
import socialnetwork.domain.Message;
import socialnetwork.service.MembruService;
import socialnetwork.service.MessageService;
import socialnetwork.utils.events.StareChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class SefController implements Observer<StareChangeEvent> {
    ObservableList<Membru> model= FXCollections.observableArrayList();
    ObservableList<Message> modelMesaje= FXCollections.observableArrayList();
    MembruService membruService;
    MessageService messageService;
    Membru selectedMembru;
    Stage accountSefStage;
    @FXML
    TableView<Membru> tableViewActivi;
    @FXML
    Button idRetragere;
    @FXML
    Button idRevenire;
    @FXML
    TableColumn<Membru,String> tableColumnNume;

    @FXML
    TableColumn<Membru,String> tableColumnRol;
    @FXML
    TableColumn<Membru,String> tableColumnStare;
    @FXML
    TableView<Message> tableViewMessage;

    @FXML
    TableColumn<Membru,String> idExpeditor;

    @FXML
    TableColumn<Membru,String> idMesaj;
    @FXML
    TableColumn<Membru,String> idOra;


    @FXML
            Button sendToOne;
    @FXML
            Button sendToAll;
    @FXML
    TextField idTextField;

    Stage introductionStage;

    public void setAttributes(MembruService membruService, MessageService messageService, Membru selectedMembru, Stage accountSefStage) {

        this.membruService=membruService;
        this.messageService=messageService;
        this.selectedMembru=selectedMembru;
        this.accountSefStage=accountSefStage;
        this.membruService.addObserver(this);
        initModel();

    }

    @FXML
    public void initialize(){
        tableColumnNume.setCellValueFactory(new PropertyValueFactory<Membru,String>("Name"));
        tableColumnRol.setCellValueFactory(new PropertyValueFactory<Membru,String>("Rol"));
        tableColumnStare.setCellValueFactory(new PropertyValueFactory<Membru,String>("Stare"));
        idExpeditor.setCellValueFactory(new PropertyValueFactory<Membru,String>("NameFrom"));
        idMesaj.setCellValueFactory(new PropertyValueFactory<Membru,String>("Message"));
        idOra.setCellValueFactory(new PropertyValueFactory<Membru,String>("Ora"));

        tableViewMessage.setItems(modelMesaje);
        tableViewActivi.setItems(model);

    }

    @Override
    public void update(StareChangeEvent stareChangeEvent) {
        initModel();
    }
    //cu ce se completeaza tabelu
    private void initModel() {
        Iterable<Membru>activi=this.membruService.getActivi();
        List<Membru> listFinala=new ArrayList<>();
        activi.forEach(membru -> {
            if(membru.getId()!=selectedMembru.getId())
                listFinala.add(membru);
        });
        modelMesaje.setAll((Collection<? extends Message>) messageService.getMessageToMembru(selectedMembru.getId()));
        model.setAll(listFinala);
    }


    public void retragereMembru() {
        Iterable<Membru>activi=this.membruService.getActivi();
        List<Membru> listFinala=new ArrayList<>();
        activi.forEach(membru -> {
            if(membru.getId()!=selectedMembru.getId())
                listFinala.add(membru);
        });
        if(listFinala.size()==0)
        {
            idRetragere.setVisible(false);
            idRevenire.setVisible(true);
        membruService.updateMembru(selectedMembru,"inactiv");
        initModel();}
        else
        {
            Alert alert= new Alert(Alert.AlertType.ERROR,"Toti membrii trebuie sa fie inactivi");
            alert.show();
        }

    }
    public void revenireMembru() {

        idRetragere.setVisible(true);
        idRevenire.setVisible(false);
        membruService.updateMembru(selectedMembru,"activ");
        initModel();

    }
    public void sendMessageToAll() {
        String mesaj=idTextField.getText();
        Iterable<Membru>activi=this.membruService.getActivi();
        List<Membru> listFinala=new ArrayList<>();
        activi.forEach(membru -> {
            if(membru.getId()!=selectedMembru.getId())
                listFinala.add(membru);
        });
        idTextField.clear();
        Message mesajFinal=new Message(selectedMembru, listFinala,mesaj, LocalDateTime.now());
        messageService.addMessage(mesajFinal);
    }


    public void sendMessageToOne() {
        String mesaj=idTextField.getText();
        idTextField.clear();
        Membru selectedMembru2=tableViewActivi.getSelectionModel().getSelectedItem();
        if(selectedMembru2!=null){
            {
                Message mesajFinal=new Message(selectedMembru, Arrays.asList(selectedMembru2),mesaj, LocalDateTime.now());
                messageService.addMessage(mesajFinal);
            }

        }else {
            Alert alert= new Alert(Alert.AlertType.ERROR,"Nu ati selectat nimic");
            alert.show();
        }

    }
}
