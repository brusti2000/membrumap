package socialnetwork.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.Membru;

import socialnetwork.service.MembruService;
import socialnetwork.service.MessageService;
import socialnetwork.utils.events.StareChangeEvent;
import socialnetwork.utils.observer.Observer;


import java.io.IOException;
import java.util.Collection;

public class IntroducereController implements Observer<StareChangeEvent> {
    MembruService membruService;

    MessageService messageService;

    ObservableList<Membru> modelMembru= FXCollections.observableArrayList();

    @FXML
    TableView<Membru> tableViewMembrii;

    @FXML
    TableColumn<Membru,String> tableColumnNume;

    @FXML
    TableColumn<Membru,String> tableColumnRol;
    @FXML
    TableColumn<Membru,String> tableColumnStare;

    Stage introductionStage;


    public void setIntroductionStage(Stage introductionStage) {
        //this.introductionStage = introductionStage;
        membruService.getAll().forEach(membru -> {
            if(membru!=null)
            {
                if(membru.getRol().equals("sef"))
                    showAccountSefStage(membru);
                else
                    showAccountMembruStage(membru);
            }
        });
    }

    @FXML
    public void initialize(){
        tableColumnNume.setCellValueFactory(new PropertyValueFactory<Membru,String>("Name"));
        tableColumnRol.setCellValueFactory(new PropertyValueFactory<Membru,String>("Rol"));
        tableColumnStare.setCellValueFactory(new PropertyValueFactory<Membru,String>("Stare"));
        tableViewMembrii.setItems(modelMembru);

    }

    public void setMembruService(MembruService membruService, Stage primaryStage) {
        this.membruService = membruService;
        this.membruService.addObserver(this);
        this.introductionStage = primaryStage;
        modelMembru.setAll((Collection<? extends Membru>) this.membruService.getAll());

    }

        public void selectItem(){
        Membru selectedItem=tableViewMembrii.getSelectionModel().getSelectedItem();
        if(selectedItem!=null){
            if(selectedItem.getRol().equals("sef"))
            {
                showAccountSefStage(selectedItem);
            }
            else showAccountMembruStage(selectedItem);
        }
    }

/*    public void selectFriendsUser(){
        UserDTO selectedUserDTO = tableViewUserDTO.getSelectionModel().getSelectedItem();
        //friendshipService.getAllFriendshipUser(selectedUserDTO.getId()).forEach(System.out::println);
        showAccountUserStage(selectedUserDTO);
    }*/

    private void showAccountMembruStage(Membru selectedMembru){ //a cui ii
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/memberView.fxml"));
            AnchorPane root = loader.load();

            Stage accountMembruStage = new Stage();
            accountMembruStage.setTitle(selectedMembru.getName()+"'s account");
           // accountMembruStage.initModality(Modality.APPLICATION_MODAL);

         /*   accountUserStage.setOnCloseRequest(event->{
                introductionStage.show();
                tableViewUserDTO.getSelectionModel().clearSelection();
            });*/
            Scene scene = new Scene(root);
            accountMembruStage.setScene(scene);
            MemberController accountMemberController=loader.getController();
            accountMemberController.setAttributes(membruService,messageService,selectedMembru,accountMembruStage);

      //    introductionStage.hide();
          accountMembruStage.show();

        }catch(IOException e){
            e.printStackTrace();
        }
    }
    private void showAccountSefStage(Membru selectedMembru){
        try{
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/sefView.fxml"));
            AnchorPane root = loader.load();

            Stage accountMembruStage = new Stage();
            accountMembruStage.setTitle(selectedMembru.getName()+"'s account SEF");
            // accountMembruStage.initModality(Modality.APPLICATION_MODAL);

         /*   accountUserStage.setOnCloseRequest(event->{
                introductionStage.show();
                tableViewUserDTO.getSelectionModel().clearSelection();
            });*/
            Scene scene = new Scene(root);
            accountMembruStage.setScene(scene);
            SefController accountMemberController=loader.getController();
            accountMemberController.setAttributes(membruService,messageService,selectedMembru,accountMembruStage);

        //    introductionStage.hide();
            accountMembruStage.show();

        }catch(IOException e){
            e.printStackTrace();
        }
    }



    public void setMessageService(MessageService messageService) {
        this.messageService=messageService;
    }

    @Override
    public void update(StareChangeEvent stareChangeEvent) {
        modelMembru.setAll((Collection<? extends Membru>) this.membruService.getAll());
    }
}
