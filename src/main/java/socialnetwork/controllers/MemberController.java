package socialnetwork.controllers;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.domain.Membru;
import socialnetwork.domain.Message;
import socialnetwork.domain.Tuple;
import socialnetwork.service.MembruService;
import socialnetwork.service.MessageService;
import socialnetwork.utils.events.StareChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.lang.reflect.Array;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MemberController implements Observer<StareChangeEvent> {
    ObservableList<Membru> model= FXCollections.observableArrayList();
    ObservableList<Message> modelMesaje= FXCollections.observableArrayList();
    MembruService membruService;
    MessageService messageService;
    Membru selectedMembru;
    Stage accountMembruStage;
    @FXML
    TableView<Membru> tableViewActivi;
    @FXML
    Button idRetragere;
    @FXML
    Button idRevenire;
    @FXML
    TableColumn<Membru,String> tableColumnNume;

    @FXML
    TableColumn<Membru,String> tableColumnRol;
    @FXML
    TableColumn<Membru,String> tableColumnStare;

    @FXML
    TableView<Message> tableViewMessage;

    @FXML
    TableColumn<Membru,String> idExpeditor;

    @FXML
    TableColumn<Membru,String> idMesaj;
    @FXML
    TableColumn<Membru,String> idOra;

    @FXML
    Button idSendToAll;
    @FXML
    TextField idTextFieldMesaj;

    Stage introductionStage;

    public void setAttributes(MembruService membruService, MessageService messageService, Membru selectedMembru, Stage accountMembruStage) {

        this.membruService=membruService;
        this.messageService=messageService;
        this.selectedMembru=selectedMembru;
        this.accountMembruStage=accountMembruStage;
        this.membruService.addObserver(this);
        this.messageService.addObserver(this);
        initModel();

    }

    @FXML
    public void initialize(){
        tableColumnNume.setCellValueFactory(new PropertyValueFactory<Membru,String>("Name"));
        tableColumnRol.setCellValueFactory(new PropertyValueFactory<Membru,String>("Rol"));
        tableColumnStare.setCellValueFactory(new PropertyValueFactory<Membru,String>("Stare"));
        idExpeditor.setCellValueFactory(new PropertyValueFactory<Membru,String>("NameFrom"));
        idMesaj.setCellValueFactory(new PropertyValueFactory<Membru,String>("Message"));
        idOra.setCellValueFactory(new PropertyValueFactory<Membru,String>("Ora"));

        tableViewMessage.setItems(modelMesaje);
       tableViewActivi.setItems(model);

    }

    @Override
    public void update(StareChangeEvent stareChangeEvent) {
        initModel();
    }
//cu ce se completeaza tabelu
    private void initModel() {
        Iterable<Membru>activi=this.membruService.getActivi();
        List<Membru> listFinala=new ArrayList<>();
        activi.forEach(membru -> {
            if(membru.getId()!=selectedMembru.getId())
                listFinala.add(membru);
        });

        model.setAll(listFinala);

       modelMesaje.setAll((Collection<? extends Message>) messageService.getMessageToMembru(selectedMembru.getId()));
    }


    public void retragereMembru() {

            idRetragere.setVisible(false);
            idRevenire.setVisible(true);
           membruService.updateMembru(selectedMembru,"inactiv");
           initModel();

    }
    public void revenireMembru() {

        idRetragere.setVisible(true);
        idRevenire.setVisible(false);
        membruService.updateMembru(selectedMembru,"activ");
        initModel();

    }


    public void sendMesaggeToAll() {
    String mesaj=idTextFieldMesaj.getText();
        Iterable<Membru>activi=this.membruService.getActivi();
        List<Membru> listFinala=new ArrayList<>();
        activi.forEach(membru -> {
            if(membru.getId()!=selectedMembru.getId())
                listFinala.add(membru);
        });
    idTextFieldMesaj.clear();
    Message mesajFinal=new Message(selectedMembru, listFinala,mesaj, LocalDateTime.now());
        messageService.addMessage(mesajFinal);
    }
}
