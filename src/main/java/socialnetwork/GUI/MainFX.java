package socialnetwork.GUI;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;
/*import socialnetwork.controllers.Intro;
import socialnetwork.controllers.MainMenuController;*/
import socialnetwork.controllers.IntroducereController;
import socialnetwork.controllers.MemberController;
import socialnetwork.controllers.SefController;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Membru;
import socialnetwork.domain.Message;
import socialnetwork.domain.validators.MembruValidator;
import socialnetwork.domain.validators.MessageValidator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.file.MembruFile;
import socialnetwork.repository.file.MessageFile;
import socialnetwork.service.MembruService;
import socialnetwork.service.MessageService;


import java.io.IOException;

public class MainFX extends Application {
    private static MembruService membruService;
  
    private static  MessageService messageService;



    @Override
    public void start(Stage primaryStage) throws Exception {
            initView(primaryStage);
            primaryStage.setWidth(600);
            primaryStage.setHeight(699);
            primaryStage.setTitle("INTRODUCTION");
       //     primaryStage.show();
    }

    public static void main(String[] args){
        String fileNameMembers= ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.echipa");

        String fileNameMessages=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.discutiiCuSefu");


        Repository<Long, Membru> membruFileRepository = new MembruFile( new MembruValidator(),fileNameMembers);

        Repository<Long, Message> messageRepository=new MessageFile(
                new MessageValidator(),fileNameMessages, membruFileRepository);


        membruService=new MembruService(membruFileRepository);

        messageService=new MessageService(messageRepository);


        launch(args);
    }
    private void initView(Stage primaryStage) throws IOException {


        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("/views/introduction.fxml"));

        AnchorPane layout = loader.load();
        primaryStage.setScene(new Scene(layout));
        IntroducereController introducereController =loader.getController();

        introducereController.setMembruService(membruService,primaryStage);
        introducereController.setMessageService(messageService);
        introducereController.setIntroductionStage(primaryStage);




    }
}
