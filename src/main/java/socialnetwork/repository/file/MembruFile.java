package socialnetwork.repository.file;

import socialnetwork.domain.Membru;
import socialnetwork.domain.validators.Validator;

import java.util.List;

public class MembruFile extends AbstractFileRepository<Long, Membru>{

    public MembruFile(Validator<Membru> validator, String fileName) {
        super(validator, fileName);
    }

    @Override
    public Membru extractEntity(List<String> attributes) {
        //TODO: implement method
        Membru user = new Membru(attributes.get(1),attributes.get(2),attributes.get(3));
        user.setId(Long.parseLong(attributes.get(0)));

        return user;
    }

    @Override
    protected String createEntityAsString(Membru entity) {
        return entity.getId()+";"+entity.getName()+";"+entity.getRol()+";"+entity.getStare();
    }


}
