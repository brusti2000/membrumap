package socialnetwork.repository.file;


import socialnetwork.domain.Membru;
import socialnetwork.domain.Message;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MessageFile extends AbstractFileRepository<Long, Message>{


    public MessageFile(Validator<Message> validator, String fileName, Repository<Long, Membru> membruRepository) {
        super(validator, fileName, membruRepository);
    }

    @Override
    public Message extractEntity(List<String> attributes) {
//        Message message=new Message(new Membru("Funny","Funny"),new ArrayList(),"Funny", LocalDateTime.now());
//        return message;
       // System.out.println(attributes);
        Long idUserEmitator=Long.parseLong(attributes.get(1));
        String list=attributes.get(2);
        String[] parts=list.split(",");
        List<Membru> idUsersReceptors=new ArrayList<>();
        for(String p:parts){
            Long id=Long.parseLong(p);
            Membru user = membruRepository.findOne(id);
            idUsersReceptors.add(user);
        }
        String textMesaj = attributes.get(3);
        LocalDateTime data=LocalDateTime.parse(attributes.get(4),Constants.DATE_TIME_FORMATER);
        Message mesaj=new Message(membruRepository.findOne(idUserEmitator), idUsersReceptors,textMesaj,data);
        mesaj.setId(Long.parseLong(attributes.get(0)));
        return mesaj;

    }

    @Override
    protected String createEntityAsString(Message entity) {
        String listTo = "";
        List<Membru> listMembrui = entity.getTo();
        for(Membru Membru : listMembrui){
            listTo += Membru.getId()+",";
        }
        listTo=listTo.substring(0,listTo.length()-1);
        String messageAttributes ="";
        messageAttributes += entity.getId()+";"
                +entity.getFrom().getId()+";"
                +listTo+";"
                +entity.getMessage()+";"
                +entity.getDate().format(Constants.DATE_TIME_FORMATER);

        return messageAttributes;
    }


}
