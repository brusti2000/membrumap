package socialnetwork.service;

import socialnetwork.domain.Membru;
import socialnetwork.domain.Message;
import socialnetwork.repository.Repository;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.StareChangeEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class MessageService implements Observable<StareChangeEvent> {
    private Repository<Long, Message> repositoryMessage;
    private List<Observer<StareChangeEvent>> observers = new ArrayList<>();
    public MessageService(Repository<Long, Message> repositoryMessage) {

        this.repositoryMessage = repositoryMessage;
    }
    public Message addMessage(Message messageParam){
        Message message = repositoryMessage.save(messageParam);
        if(message==null) {
            notifyObserver(new StareChangeEvent());
        }
        return message;
    }

    /**
     * @param id - the id of the user
     * @return a list that contains only those messages addressed to the user that has id
     */
    public Iterable<Message> getMessageToMembru(Long id){
        Iterable<Message> listIterableAllMessage=repositoryMessage.findAll();
        List<Message> filterList=new ArrayList<>();
        listIterableAllMessage.forEach(message->{
            List<Membru> listUsersTo = message.getTo();
            AtomicBoolean b=new AtomicBoolean(false);
            listUsersTo.forEach(users->{
                if(users.getId().equals(id)){
                    b.set(true);
                }
            });
            if(b.get()){
                filterList.add(message);
            }
        });
        return filterList;
    }

    public Message getMessage(Long idMessage){
        return repositoryMessage.findOne(idMessage);
    }

    @Override
    public void addObserver(Observer<StareChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<StareChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObserver(StareChangeEvent t) {
        observers.stream().forEach(obs->obs.update(t));
    }
}
