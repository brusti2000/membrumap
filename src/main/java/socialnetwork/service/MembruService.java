package socialnetwork.service;

import socialnetwork.domain.Membru;
import socialnetwork.domain.Tuple;
import socialnetwork.repository.Repository;
import socialnetwork.service.validator.ValidatorMembruService;
import socialnetwork.service.validator.Validator;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.StareChangeEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.lang.reflect.Member;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MembruService implements Observable<StareChangeEvent> {
    private final Repository<Long, Membru> repoMembru;
    private List<Observer<StareChangeEvent>> observers = new ArrayList<>();
    private final Validator<Membru> validatorMembruService=new ValidatorMembruService<>();


    public MembruService(Repository<Long, Membru> repoMembru) {
        this.repoMembru = repoMembru;
    }

    /**
     * @param Membru - type Membru, entity that has to be added
     * @return user of type Membru
     */
    public Membru addMembru(Membru Membru) {
        Membru user = repoMembru.save(Membru);
        //facem validarea in functie de ce ne returneaza .save
        validatorMembruService.validateAdd(user);
        return user;
    }

    /**
     * method that removes the user with id given as a parameter and
     * its friendships
     * @param id - Long, the unique identifier of the user to be deleted
     * @return user that has been removed
     */
    public Membru deleteMembru(Long id) {
        //3;4
        //3;5
        //6;3
        //sterge in toate prieteniile care il contin pe 3
        Membru membru = repoMembru.delete(id);
     //   validatorMembruService.validateDelete(membru);
        if(membru!=null) {
            notifyObserver(new StareChangeEvent(ChangeEventType.DELETE, membru));
        }
        return membru;
    }

    /**
     * @return all the users
     */
    public Iterable<Membru> getAll(){
        return repoMembru.findAll();
    }

    public Membru findOne(Long ID){
        return repoMembru.findOne(ID);
    }

    public Iterable<Membru> getMembriiFaraSef(){
        Iterable<Membru>listMembrii= repoMembru.findAll();
        List<Membru>filter=new ArrayList<>();
        listMembrii.forEach(membru->{
            if(membru.getRol().equals("membru"))
            filter.add(membru);
        });
        return filter;
    }
    public Iterable<Membru> getActivi(){
        Iterable<Membru>listMembrii= repoMembru.findAll();
        List<Membru>filter=new ArrayList<>();
        listMembrii.forEach(membru->{
            if(membru.getStare().equals("activ"))
                filter.add(membru);
        });
        return filter;
    }

    public void updateMembru( Membru membru,String status){
        Membru fr = deleteMembru(membru.getId());
        fr.setStare(status);

        fr= addMembru(fr);
        notifyObserver(new StareChangeEvent(ChangeEventType.UPDATE,fr));

    }

    public Membru getSef(){
        Iterable<Membru>listMembrii= repoMembru.findAll();

        for(Membru membru:listMembrii)
            if(membru.getRol().equals("sef")) {
                return membru;

    }
        return null;
    }

    @Override
    public void addObserver(Observer<StareChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<StareChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObserver(StareChangeEvent t) {
        observers.stream().forEach(obs->obs.update(t));
    }
}
