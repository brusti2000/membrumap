package socialnetwork.service.validator;

import socialnetwork.domain.validators.ValidationException;

/**
 * define a class that implements a generic interface, Validator
 * @param <T> -
 */
public class ValidatorMembruService<T> implements Validator<T>{
    /**
     * @param entity - entity of type T
     * @throws ValidationException if he user already exists
     */
    @Override
    public void validateAdd(T entity) throws ValidationException {
        if(entity!=null){
            throw new ValidationException("Utilizatorul exista deja\n!");
        }
    }

    /**
     * @param entity - entity of type T
     * @throws ValidationException if the user does not exists
     */
    @Override
    public void validateDelete(T entity) throws ValidationException {
        if(entity==null){
            throw new ValidationException(("Utilizatorul nu exista!\n"));
        }
    }
}
